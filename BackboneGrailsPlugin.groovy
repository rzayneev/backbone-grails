class BackboneGrailsPlugin {
    def version = "0.5"
    def grailsVersion = "2.3 > *"
    def title = "Backbone Plugin"
    def author = "Ramil Zayneev"
    def authorEmail = "ramilzrr@gmail.com"
    def description = "Provides [Backbone JS](http://backbonejs.org/) and [Underscore JS](http://underscorejs.org/) resources"
    def documentation = "http://grails.org/plugin/backbone"
    def developers = [[ name: "Ramil Zayneev", email: "ramilzrr@gmail.com" ]]
    def issueManagement = [ system: "Bitbucket", url: "https://bitbucket.org/rzayneev/backbone-grails/issues" ]
    def scm = [ url: "https://bitbucket.org/rzayneev/backbone-grails/" ]
}
