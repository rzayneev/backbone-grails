import grails.util.Environment

modules = {
//   Underscore.js 1.6.0
    'underscore' {
        dependsOn 'jquery'
        resource url:[plugin: 'backbone', dir:'js/', file: 'underscore'+(Environment.isDevelopmentMode() ? '.js' : '-min.js')],
                 disposition: 'defer', exclude:'minify'
    }
//     Backbone.js 1.1.2
    'backbone' {
        dependsOn 'underscore'
        resource url:[plugin: 'backbone', dir:'js/', file: 'backbone'+(Environment.isDevelopmentMode() ? '.js' : '-min.js')],
                 disposition: 'defer', exclude:'minify'
    }
}
