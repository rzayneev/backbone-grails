# Backbone js resources plugin v.0.5 #

Provides [Backbone JS](http://backbonejs.org/) and [Underscore JS](http://underscorejs.org/) resources for Grails.

Use the resources to include the backbone module in your gsp.

```
#!

<r:require module="backbone" />

```

## Scripts ##

For copying original files just run

```
#!

grails install-backbone

```

Plugin also providing additional commands:


```
#!

grails create-backbone-model <Model name>
grails create-backbone-view <View name>
grails create-backbone-router <Router name>
grails create-backbone-collection <Collection name> <Model name>

```


Structure location of generated files:

* web-app/js/backbone/ (backbone.js and underscore.js location)
* web-app/js/backbone/models/
* web-app/js/backbone/views/
* web-app/js/backbone/collections/
* web-app/js/backbone/routers/


