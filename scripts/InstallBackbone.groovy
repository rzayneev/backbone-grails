includeTargets << grailsScript("_GrailsEvents")

target(installBackbone: "Download backbone") {
	downloadOne 'Backbone', 'backbone', 'http://backbonejs.org'
	downloadOne 'Underscore', 'underscore', 'http://underscorejs.org'
}

void downloadOne(String upperName, String name, String site) {

	event("StatusUpdate", ["Downloading $upperName"])

	String dir = "$basedir/web-app/js/backbone"
	mkdir(dir: dir)

	["${name}.js", "${name}-min.js"].each { file ->
		get(dest: "$dir/$file", src: "$site/$file", verbose: true)
	}

	event("StatusUpdate", ["Success: $upperName installed!"])
}

setDefaultTarget(installBackbone)
