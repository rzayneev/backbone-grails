includeTargets << new File(backbonePluginDir, 'scripts/_BackboneCommon.groovy')

target(createBackboneView: "Create backbone view") {

    def viewName = argsMap.params[0]

    renderBackboneTemplate('views', viewName, [viewName: viewName])
}

setDefaultTarget(createBackboneView)
