includeTargets << new File(backbonePluginDir, 'scripts/_BackboneCommon.groovy')

target(createBackboneRouter: "Create backbone router") {

    def routerName = argsMap.params[0]

    renderBackboneTemplate('routers', routerName, [routerName: routerName])
}

setDefaultTarget(createBackboneRouter)
