includeTargets << grailsScript("_GrailsEvents")

def backboneBaseDir = "$basedir/web-app/js/backbone"

renderBackboneTemplate = {type, name, vars ->
    def file = new File(backboneBaseDir + "/" + type + "/" + name.toLowerCase() + '.js')
    file.parentFile?.mkdirs()

    ant.copy(file: "$backbonePluginDir/src/templates/backbone-${type}.js.template",
             tofile: file.path, verbose: true, overwrite: true) {
        filterset {
            vars.each {
                filter token: it.key, value: it.value
            }
        }
    }

    event('StatusUpdate', ["Backbone ${type}: ${name} ready!"])
}