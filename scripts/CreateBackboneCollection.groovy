includeTargets << new File(backbonePluginDir, 'scripts/_BackboneCommon.groovy')

target(createBackboneCollection: "Create backbone collection") {

    try {
        def collectionName = argsMap.params[0]
        def modelName = argsMap.params[1]
        renderBackboneTemplate('collections', collectionName, [collectionName: collectionName, modelName: modelName])

    } catch (e) {
        println e.message
    }
}

setDefaultTarget(createBackboneCollection)
